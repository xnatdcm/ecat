/*
 * ecat-io: org.nrg.ecat.var.RealListVariableTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.var;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;

import org.nrg.ecat.Header;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public class RealListVariableTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.ecat.var.RealListVariable#RealListVariable(org.nrg.ecat.Header.Type, java.lang.String, long, int)}.
	 */
	public void testRealListVariable() {
		final EcatVariable rlv = new RealListVariable(Header.MAIN, "TEST_VAR", 16, 3);
		assertEquals(Header.MAIN, rlv.getHeaderType());
		assertEquals("TEST_VAR", rlv.getName());
		assertTrue(16 == rlv.getOffset());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.RealListVariable#readValue(java.io.InputStream)}.
	 */
	public void testReadValue() throws IOException {
		final byte[] bytes = RealVariableTest.toBytes(new int[] {
				Float.floatToRawIntBits(-1.1f),
				Float.floatToRawIntBits(0.0f),
				Float.floatToRawIntBits(1.1f),
				Float.floatToRawIntBits(2.2f),
				Float.floatToRawIntBits(3.3f),
				Float.floatToRawIntBits(4.4f),
				Float.floatToRawIntBits(5.5f),
		});
		
		final EcatVariable rlv = new RealListVariable(Header.MAIN, "TEST_VAR", 32, 3);
		final InputStream  in  = new ByteArrayInputStream(bytes);
		try {
			final Iterator i1 = ((Collection)rlv.readValue(in)).iterator();
			assertEquals(new Float(-1.1), i1.next());
			assertEquals(new Float(0.0), i1.next());
			assertEquals(new Float(1.1), i1.next());
			assertFalse(i1.hasNext());
			
			final Iterator i2 = ((Collection)rlv.readValue(in)).iterator();
			assertEquals(new Float(2.2), i2.next());
			assertEquals(new Float(3.3), i2.next());
			assertEquals(new Float(4.4), i2.next());
			assertFalse(i2.hasNext());
			try {
				rlv.readValue(in);
				fail("expected IOException on EOF");
			} catch (IOException ok) {}
		} finally {
			in.close();
		}
	}
	
	/**
	 * Test method for {@link org.nrg.ecat.var.RealListVariable#toString()}.
	 */
	public void testToString() {
		final EcatVariable v = new RealListVariable(Header.MAIN, "V", 8, 4);
		assertEquals("8 V(4) Real*4", v.toString());
	}
}
