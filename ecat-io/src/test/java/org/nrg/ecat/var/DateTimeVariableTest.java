/*
 * ecat-io: org.nrg.ecat.var.DateTimeVariableTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.var;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import org.nrg.ecat.Header;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public class DateTimeVariableTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.ecat.var.DateTimeVariable#DateTimeVariable(org.nrg.ecat.Header.Type, java.lang.String, long)}.
	 */
	public void testDateTimeVariable() {
		final EcatVariable dtv = new DateTimeVariable(Header.MAIN, "TEST_VAR", 32);
		assertEquals(Header.MAIN, dtv.getHeaderType());
		assertEquals("TEST_VAR", dtv.getName());
		assertTrue(32 == dtv.getOffset());
	}

	/**
	 * Test method for {@link org.nrg.ecat.var.DateTimeVariable#readValue(java.io.InputStream)}.
	 */
	public void testReadValue() throws IOException {
		final Date now = Calendar.getInstance().getTime();
		final long lnow = now.getTime() / 1000L;
		final byte[] bytes = new byte[] {
			(byte)((lnow & 0xff000000) >> 24),
			(byte)((lnow & 0x00ff0000) >> 16),
			(byte)((lnow & 0x0000ff00) >> 8),
			(byte)(lnow & 0x000000ff),
		};
		final EcatVariable dtv = new DateTimeVariable(Header.MAIN, "TEST_VAR", 16);
		final InputStream  in  = new ByteArrayInputStream(bytes);
		try {
			// Can't do simple data conversion, because the ECAT representation
			// truncates out millseconds.
			final Date then = (Date)dtv.readValue(in);
			assertTrue(lnow == then.getTime() / 1000L);
			try {
				dtv.readValue(in);
				fail("Expected EOF");
			} catch (IOException ok) {}
		} finally {
			in.close();
		}
	}
	
	/**
	 * Test method for {@link org.nrg.ecat.var.DateTimeVariable#toString()}.
	 */
	public void testToString() {
		final EcatVariable dtv = new DateTimeVariable(Header.MAIN, "V", 24);
		assertEquals("24 V Integer*4", dtv.toString());
	}
}
