/*
 * ecat-io: org.nrg.ecat.HeaderModification
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public interface HeaderModification extends Comparable {
	Header.Type getHeaderType();
	int getOffset();
	int modify(OutputStream to, InputStream from) throws IOException;
}
