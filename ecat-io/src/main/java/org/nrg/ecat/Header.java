/*
 * ecat-io: org.nrg.ecat.Header
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat;

import java.util.Collection;
import java.util.SortedMap;

import org.nrg.ecat.var.EcatVariable;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public interface Header {
	Type getType();
	Object getValue(EcatVariable variable);
	SortedMap getVariableValues();
	Collection getVariables();
	
	public final static class Type implements Comparable {
		private final String name;
		private final Integer code;
		
		private Type(final String name, final int code) {
			this.name = name;
			this.code = new Integer(code);
		}
		
		public String getName() { return name; }
		
		/*
		 * (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(final Object o) {
			return code.compareTo(((Type)o).code);
		}
		
		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return super.toString() + " " + name;
		}
	}
	
	public final static Type MAIN = new Type("Main", 0);
}
