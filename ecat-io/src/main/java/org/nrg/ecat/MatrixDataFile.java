/*
 * ecat-io: org.nrg.ecat.MatrixDataFile
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.nrg.ecat.var.EcatVariable;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public final class MatrixDataFile implements Comparable {
	private final File f;
	private final MatrixData data;
	
	/**
	 * 
	 */
	public MatrixDataFile(final File f) throws IOException {
		this.f = getFullPathFile(f);
		final InputStream in = new FileInputStream(this.f);
		try {
			this.data = new MatrixData(in);
		} finally {
			in.close();
		}
	}
	
	private static File getFullPathFile(final File f) {
		try {
			return f.getCanonicalFile();
		} catch (IOException e) {
			return f.getAbsoluteFile();
		}
	}
	
	public int compareTo(final MatrixDataFile o) {
		return data.compareTo(o.data);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(final Object o) {
		return compareTo((MatrixDataFile)o);
	}
	
	private Object getMainHeader(final EcatVariable v) {
		return data.getMainHeader().getValue(v);
	}
	
	public Date getDate() {
		return (Date)getMainHeader(MainHeader.SCAN_START_TIME);
	}
	
	public String getDescription() {
		return getMainHeader(MainHeader.STUDY_DESCRIPTION).toString();
	}
	
	public File getFile() { return f; }
	
	public String getName() { return f.getName(); }
	
	public String getPath() { return f.getPath(); }
	
	public String getPatientID() {
		return getMainHeader(MainHeader.PATIENT_ID).toString();
	}
	
	public long getSize() { return f.length(); }
}
