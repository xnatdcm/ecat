/*
 * ecat-io: org.nrg.ecat.var.RealListVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.var;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;

import org.nrg.ecat.HeaderModification;
import org.nrg.ecat.Header.Type;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public final class RealListVariable extends AbstractVariable {
	private final EcatVariable realVar;
	private final int          length;
	
	/**
	 * @param type
	 * @param name
	 * @param offset
	 * @param length
	 */
	public RealListVariable(final Type type, final String name, final int offset, final int length) {
		super(type, name, offset);
		this.realVar = new RealVariable(type, name + " component", offset);
		this.length = length;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.AbstractVariable#toString()
	 */
	public String toString() {
		final StringBuffer sb = new StringBuffer(super.toString());
		return sb.append("(").append(length).append(") Real*4").toString();
	}
	
	/* (non-Javadoc)
	 * @see org.nrg.ecat.var.EcatVariable#readValue(java.io.InputStream)
	 */
	public Object readValue(final InputStream in) throws IOException {
		final List list = new ArrayList(length);
		for (int i = 0; i < length; i++) {
			list.add(realVar.readValue(in));
		}
		return list;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.EcatVariable#createClearModification()
	 */
	public HeaderModification createClearModification() {
		return createClearModification(4*length);
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.EcatVariable#createValueModification(java.lang.Object)
	 */
	public HeaderModification createValueModification(final Object value) {
		throw new UnsupportedOperationException();	// TODO: implement
	}
}
