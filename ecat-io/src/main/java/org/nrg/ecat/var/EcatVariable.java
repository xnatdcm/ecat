/*
 * ecat-io: org.nrg.ecat.var.Variable
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.var;

import java.io.IOException;
import java.io.InputStream;

import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.ecat.Header;
import org.nrg.ecat.HeaderModification;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public interface EcatVariable extends Variable, Comparable {
	Header.Type getHeaderType();
	int getOffset();
	String getName();
	Object readValue(InputStream in) throws IOException;
	HeaderModification createClearModification();
	HeaderModification createValueModification(Object value);
}
