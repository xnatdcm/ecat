/*
 * ecat-io: org.nrg.ecat.var.RealVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.var;

import java.io.IOException;
import java.io.InputStream;

import org.nrg.ecat.HeaderModification;
import org.nrg.ecat.Header.Type;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public final class RealVariable extends AbstractVariable {

	/**
	 * @param type
	 * @param name
	 * @param offset
	 */
	public RealVariable(final Type type, final String name, final int offset) {
		super(type, name, offset);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.AbstractVariable#toString()
	 */
	public String toString() {
		final StringBuffer sb = new StringBuffer(super.toString());
		return sb.append(" Real*4").toString();
	}

	/* (non-Javadoc)
	 * @see org.nrg.ecat.var.EcatVariable#readValue(java.io.InputStream)
	 */
	public Object readValue(final InputStream in) throws IOException {
		return new Float(Float.intBitsToFloat(read4ByteInt(in)));
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.EcatVariable#createClearModification()
	 */
	public HeaderModification createClearModification() {
		return createClearModification(4);
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.nrg.ecat.var.EcatVariable#createValueModification(java.lang.Object)
	 */
	public HeaderModification createValueModification(final Object value) {
		throw new UnsupportedOperationException();	// TODO: implement
	}
}
