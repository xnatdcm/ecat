/*
 * ecat-io: org.nrg.ProgressListener
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public interface ProgressListener {
	void setMessage(String message);
	void incrementProgress(int increment);
	void incrementTaskSize(int increment);
}
