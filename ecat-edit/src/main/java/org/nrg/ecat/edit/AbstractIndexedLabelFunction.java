/*
 * ecat-edit: org.nrg.ecat.edit.AbstractIndexedLabelFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.edit;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public abstract class AbstractIndexedLabelFunction implements ScriptFunction {
    protected abstract boolean isAvailable(String label) throws ScriptEvaluationException;

    private Value getFormat(final List values) throws ScriptEvaluationException {
        try {
            return (Value) values.get(0);
        } catch (IndexOutOfBoundsException e) {
            try {
                throw new ScriptEvaluationException(getClass().getField("name").get(null) + " requires format argument");
            } catch (IllegalArgumentException | SecurityException | NoSuchFieldException | IllegalAccessException e1) {
                throw new RuntimeException(e1);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.ecat.edit.ScriptFunction#apply(java.util.List)
     */
    public Value apply(final List args) throws ScriptEvaluationException {
        final Value format = getFormat(args);
        return new AbstractEcatMizerValue() {
            private NumberFormat buildFormatter(final int len) {
                final StringBuilder sb = new StringBuilder();
                for (int i = 0; i < len; i++) {
                    sb.append("0");
                }
                return new DecimalFormat(sb.toString());
            }

            private String valueFor(final Object fo) throws ScriptEvaluationException {
                final String format = fo.toString();
                final int    offset = format.indexOf('#');
                int          end    = offset;
                while ('#' == format.charAt(end)) {
                    end++;
                }
                final NumberFormat  nf = buildFormatter(end - offset);
                final StringBuilder sb = new StringBuilder(format);
                for (int i = 0; true; i++) {
                    final String label = sb.replace(offset, end, nf.format(i)).toString();
                    if (isAvailable(label)) {
                        return label;
                    }
                }
            }

            @Override
            public String on(Map<Integer, String> map) throws ScriptEvaluationException {
                return valueFor(format.on(map));
            }

            @Override
            public Set<Variable> getVariables() {
                return format.getVariables();
            }

            @Override
            public void replace(final Variable variable) {
                format.replace(variable);
            }

            @Override
            public SortedSet<Long> getTags() {
                return null;
            }
        };
    }
}
