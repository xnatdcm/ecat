/*
 * ecat-edit: org.nrg.ecat.edit.ScriptApplicator
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.edit;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.ecat.edit.fn.*;
import org.nrg.framework.utilities.GraphUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public final class ScriptApplicator {
    private static final Logger logger = LoggerFactory.getLogger(ScriptApplicator.class);
    private final EditECATASTParser astParser;

    public ScriptApplicator(final InputStream script, final Map<String, ? extends ScriptFunction> functions) throws IOException, ScriptEvaluationException {
        if (null == script) {
            astParser = null;
        } else {
            try {
                logger.trace("setting up ECAT edit lexer");
                final EditECATLexer lexer = new EditECATLexer(new ANTLRInputStream(script));
                logger.trace("setting up ECAT edit parser");
                final EditECATParser parser = new EditECATParser(new CommonTokenStream(lexer));
                logger.trace("parsing ECAT edit script to AST");
                final EditECATParser.script_return sr = parser.script();

                final CommonTree ast = (CommonTree) sr.getTree();
                if (null == ast) {
                    logger.trace("unparseable");
                    astParser = null;
                } else {
                    astParser = new EditECATASTParser(new CommonTreeNodeStream(ast));
                    astParser.setFunction(Substring.name, new Substring());
                    astParser.setFunction(Format.name, new Format());
                    astParser.setFunction(Lowercase.name, new Lowercase());
                    astParser.setFunction(Replace.name, new Replace());
                    astParser.setFunction(Match.name, new Match());
                    for (final String functionName : functions.keySet()) {
                        final ScriptFunction function = functions.get(functionName);
                        logger.trace("adding function {}", functionName);
                        astParser.setFunction(functionName, function);
                    }
                    logger.trace("parsing ECAT edit AST");
                    astParser.script();
                    logger.trace("script-defined variables: {}", astParser.getVariables());
                }
            } catch (RecognitionException e) {
                logger.error("error parsing ECAT script", e);
                throw new ScriptEvaluationException("error parsing script", e);
            }
        }
    }

    public Variable getVariable(final String label) {
        return astParser.getVariable(label);
    }

    public Map getVariables() {
        return null == astParser ? Collections.EMPTY_MAP : astParser.getVariables();
    }

    public List<Variable> getSortedVariables(final Collection excluding) {
        if (null == astParser) {
            return EMPTY_VARIABLES;
        }

        final Map<Variable, Collection<Variable>> graph = new LinkedHashMap<>();
        for (final Variable variable : astParser.getVariables().values()) {
            if (!excluding.contains(variable)) {
                final Value          initialValue = variable.getInitialValue();
                final List<Variable> dependencies = new ArrayList<>();
                if (null != initialValue) {
                    for (final Variable initialVariable : initialValue.getVariables()) {
                        if (!excluding.contains(initialVariable.getName())) {
                            dependencies.add(initialVariable);
                        }
                    }
                }
                graph.put(variable, dependencies);
            }
        }
        return GraphUtils.topologicalSort(graph);
    }

    @SuppressWarnings("unused")
    public List getSortedVariables(final String[] excluding) {
        return getSortedVariables(Arrays.asList(excluding));
    }

    public List getSortedVariables() {
        return getSortedVariables(Collections.EMPTY_LIST);
    }

    private static final List<Variable> EMPTY_VARIABLES = new ArrayList<>(Value.EMPTY_VARIABLES);
}
