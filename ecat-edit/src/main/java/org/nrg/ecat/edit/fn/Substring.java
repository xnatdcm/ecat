/*
 * ecat-edit: org.nrg.ecat.edit.fn.Substring
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.edit.fn;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.dicom.mizer.values.IntegerValue;
import org.nrg.dicom.mizer.values.SubstringValue;
import org.nrg.dicom.mizer.values.Value;

import java.util.List;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public final class Substring implements ScriptFunction {
    public static final String name = "substring";

    @Override
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
        final Value value = args.get(0);
        final int   start;
        final int   end;
        try {
            start = ((IntegerValue) args.get(1)).getValue();
            end = ((IntegerValue) args.get(2)).getValue();
        } catch (ClassCastException e) {
            throw new ScriptEvaluationException("substring[ string, start(integer), end(integer)]");
        }
        return new SubstringValue(value, start, end);
    }
}
