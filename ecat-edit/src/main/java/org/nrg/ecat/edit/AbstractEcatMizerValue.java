/*
 * ecat-edit: org.nrg.ecat.edit.AbstractEcatMizerValue
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.ecat.edit;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.AbstractMizerValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.Variable;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * Provides a base class for Mizer {@link Value} objects. The only method implemented here is the {@link
 * Value#on(DicomObjectI) DICOM object evaluation method}, which is not supported for ECAT and throws an exception when
 * called.
 */
public abstract class AbstractEcatMizerValue extends AbstractMizerValue {
    @Override
    public abstract Set<Variable> getVariables();

    @Override
    public abstract void replace(final Variable variable);

    @Override
    public abstract SortedSet<Long> getTags();

    @Override
    public abstract String on(final Map<Integer, String> m) throws ScriptEvaluationException;

    /**
     * Throws an <b>UnsupportedOperationException</b>. This implementation does not support DICOM objects.
     *
     * @param dicomObject The DICOM object that can't be handled.
     *
     * @return Nothing, this method will always throw an exception.
     */
    @Override
    public String on(final DicomObjectI dicomObject) throws ScriptEvaluationException {
        throw new UnsupportedOperationException();
    }
}
