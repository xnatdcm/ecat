/*
 * ecat-edit: org.nrg.ecat.edit.fn.Replace
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat.edit.fn;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.scripts.ScriptFunction;
import org.nrg.dicom.mizer.values.ReplaceValue;
import org.nrg.dicom.mizer.values.Value;

import java.util.List;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 */
public final class Replace implements ScriptFunction {
    public static final String name = "replace";

    @Override
    public Value apply(final List<? extends Value> args) throws ScriptEvaluationException {
        if (args == null || args.size() < 3) {
            throw new ScriptEvaluationException(name + " takes three arguments [original, pre, post]");
        }

        return new ReplaceValue(args.get(0), args.get(1), args.get(2));
    }
}
