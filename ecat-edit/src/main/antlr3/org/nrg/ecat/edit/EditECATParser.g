parser grammar EditECATParser;

options {
	output = AST;
	tokenVocab = EditECATLexer;
}

tokens {
	FUNCTION;
	INITIALIZE;
}

@header {
	package org.nrg.ecat.edit;
}
	
script	:	(NEWLINE!* statement)* NEWLINE!* EOF!;

statement
	:	action
	|	initialization
	|	description
	;

action	:	echo;

echo	:	ECHO value -> ^(ECHO value)
	|	ECHO
	;

initialization
	:	ID ASSIGN value -> ^(INITIALIZE ID value)
	;
	
description
	:	DESCRIBE ID STRING -> ^(DESCRIBE ID STRING)
	|	DESCRIBE ID HIDDEN -> ^(HIDDEN ID)
	|	EXPORT ID STRING   -> ^(EXPORT ID STRING)
	;
	
term	:	STRING
	|	NUMBER
	|	ID LEFT termlist RIGHT -> ^(FUNCTION ID termlist)
	|	ID
	;
	
termlist 
	:	term (COMMA! term)*
	;
	
value 	:	term
	;
